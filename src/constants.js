// stores environment variables

const serverAPI = "https://datequote-backend.herokuapp.com";

const localAPI = "http://localhost:5000";

const config = process.env.NODE_ENV === "development" ? localAPI : serverAPI;
// const config = process.env.api || localAPI;
module.exports = {
  API: config
};
