import { instance as axios } from "./AxiosConfig";
const config = require("../constants");

export const getQuotesByCategory = async category => {
  console.log(process.env.NODE_ENV + "env");

  const result = await axios.get(`/api/${category}`);
  console.log(result.data.quotes);
  return result.data.quotes;
};
