import axios from "axios";
const config = require("../constants");
export const instance = axios.create({
  baseURL: config.API,
  timeout: 5000
});
