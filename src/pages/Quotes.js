import React, { Component } from "react";

import Quote from "./SingleQuote";

const shuffle = require("shuffle-array");

const Api = require("../Api/Routes");

class Quotes extends Component {
  state = {
    quotes: []
  };
  constructor(props) {
    super(props);
  }

  getQuotes = async () => {
    const category = this.props.category;
    try {
      const result = await Api.getQuotesByCategory(category);
      shuffle(result);

      this.setState({ quotes: result });
      if (this.state.quotes == null) window.location.reload();
    } catch (error) {
      //   window.location.reload();
    }
  };

  componentDidMount() {
    this.getQuotes();
  }

  componentWillReceiveProps(nextProps) {}

  componentWillUpdate(nextProps, nextState) {}

  componentDidUpdate(prevProps, prevState) {}

  render() {
    return (
      <div>
        <header>
          <h2>{this.props.category + " "} quotes</h2>
        </header>
        {this.state.quotes.map(quote => (
          <Quote key={quote.id} quoteInfo={quote} />
        ))}
      </div>
    );
  }
}

Quotes.propTypes = {};
export default Quotes;
