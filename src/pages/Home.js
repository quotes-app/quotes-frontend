import React, { Component } from "react";
import { Link } from "react-router-dom";

class Home extends Component {
  componentWillMount() {}

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  componentWillUpdate(nextProps, nextState) {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillUnmount() {}

  render() {
    return (
      <div className="row ">
        <div className="col-md-12 ">
          <Link style={linkStyle} className="card col-md-12" to="/programming">
            <div>
              <div className="card-body ">
                <h3 className="card-title">Programming Quotes</h3>
              </div>
            </div>
          </Link>

          <Link style={linkStyle} className="card col-md-12" to="/movies">
            <div>
              <div className="card-body ">
                <h3 className="card-title">Movie Quotes</h3>
              </div>
            </div>
          </Link>

          <Link style={linkStyle} className="card col-md-12" to="/love">
            <div>
              <div className="card-body ">
                <h3 className="card-title">Love Quotes</h3>
              </div>
            </div>
          </Link>

          <Link style={linkStyle} className="card col-md-12" to="/motivational">
            <div>
              <div className="card-body ">
                <h3 className="card-title">Motivational Quotes</h3>
              </div>
            </div>
          </Link>
        </div>
      </div>
    );
  }
}

const linkStyle = {
  textDecoration: "none"
};
Home.propTypes = {};

export default Home;
