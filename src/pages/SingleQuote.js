import React, { Component } from "react";

class componentName extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {}

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  shouldComponentUpdate(nextProps, nextState) {}

  componentWillUpdate(nextProps, nextState) {}

  componentDidUpdate(prevProps, prevState) {}

  componentWillUnmount() {}

  render() {
    return (
      <div className="card col-10 offset-1 mt-5">
        <br />
        <div className="card-body col-12 ">
          <strong>
            <p>
              <h2>
                <blockquote>{this.props.quoteInfo.quote}</blockquote>
              </h2>
            </p>
          </strong>
          <p>- {this.props.quoteInfo.author}</p>
        </div>
      </div>
    );
  }
}

componentName.propTypes = {};

export default componentName;
