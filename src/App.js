import { BrowserRouter as Router, Route } from "react-router-dom";
import React, { Component } from "react";
import "./App.css";

import Header from "./layouts/Header";
import Home from "./pages/Home";
import Quotes from "./pages/Quotes";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container-fluid">
          <Router>
            <div className="row">
              <Header />
            </div>

            <div style={styles}>
              <Route exact path="/" component={Home} />

              <Route
                exact
                path="/programming"
                render={props => <Quotes category={"programming"} />}
              />

              <Route
                exact
                path="/movie"
                render={props => <Quotes category="Movie" />}
              />

              <Route path="/love" component={Quotes} />

              <Route
                path="/motivational"
                render={props => <Quotes category="Motivational" />}
              />
            </div>
          </Router>
        </div>
      </div>
    );
  }
}
const styles = {
  marginTop: "100px"
};
export default App;
